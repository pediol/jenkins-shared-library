#!/usr/bin/env groovy

def call(){
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: '429714eb-541b-4b25-a3a0-6142668664c3', passwordVariable: 'PWD', usernameVariable: 'USER')]) {

        sh 'docker build -t olical22/repository.1:my-app-4.0 .'
        sh "echo $PWD | docker login -u $USER --password-stdin"
        sh docker push olical22/repository.1-my-app-4.0
    }
}

